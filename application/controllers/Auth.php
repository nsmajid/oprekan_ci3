<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index() {

        if ($this->input->post('submitLogin')) {

            $input = $this->input->post();

            extract($input);


            $user = $this->db->get_where('user', ['username' => $username])->row();


            if ($user) {

                if (password_verify($password, $user->password)) {

                    $data = [
                        'username' => $user->username,
                        'id_user' => $user->id_user,
                        'isadmin' => $user->isadmin,
                        'level' => $user->level_name
                    ];
                    $this->session->set_userdata($data);


                    redirect(base_url());
                } else {
                    $this->session->set_flashdata('notif', true);
                    $this->session->set_flashdata('notif_title', 'GAGAL');
                    $this->session->set_flashdata('notif_message', 'Password tidak tepat.');
                    $this->session->set_flashdata('notif_class', 'danger');
                    $this->session->set_flashdata('notif_icon', 'fas fa-ban');
                    redirect(base_url());
                }
            } else {
                $this->session->set_flashdata('notif', true);
                $this->session->set_flashdata('notif_title', 'GAGAL');
                $this->session->set_flashdata('notif_message', 'Username tidak ditemukan.');
                $this->session->set_flashdata('notif_class', 'danger');
                $this->session->set_flashdata('notif_icon', 'fas fa-ban');
                redirect(base_url());
            }
        } else {
            if ($this->session->userdata('id_user')) {
                redirect(base_url('dashboard'));
            }

            $this->load->view('auth/vlogin', NULL);
        }
    }

    private function suspend_check($suspend) {
        
    }

    private function active_check($active) {
        
    }

    public function logout() {
        $user_data = $this->session->all_userdata();

        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }

        $this->session->set_flashdata('notif', true);
        $this->session->set_flashdata('notif_title', 'BERHASIL');
        $this->session->set_flashdata('notif_message', 'Sampai Jumpa Lagi.');
        $this->session->set_flashdata('notif_class', 'success');
        $this->session->set_flashdata('notif_icon', 'fas fa-check');
        redirect(base_url());
    }

}
