<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."third_party/Autoloader.php");

use PhpOffice\PhpWord\Autoloader;
Autoloader::register();

class Basic_phpword extends CI_Controller {

    public function __construct() {
        parent::__construct();

        login_check();
        
    }

    public function index() {
      
        $data['breadcrumb'] = array('Office', 'PHPWord', 'Basic');
        $data['title'] = 'Basic PHPWord';

        $data['content'] = 'basic_phpword/vindex';
        $this->load->view('template/vtemplate', $data);
    }

    function download() {
		
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
		
		$section = $phpWord->addSection();
		
		$section->addText(
			'"Learn from yesterday, live for today, hope for tomorrow. '
				. 'The important thing is not to stop questioning." '
				. '(Albert Einstein)'
		);
        
		
		$filename = date('Y-m-d H:i:s') . '.docx';		
		
		header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');
    }

}
