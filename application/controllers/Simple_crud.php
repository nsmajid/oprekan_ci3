<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Simple_crud extends CI_Controller {

    public function __construct() {
        parent::__construct();

        login_check();

        $this->load->model('Magama');
    }

    public function index() {
        $data['agama'] = $this->Magama->all();
        $data['breadcrumb'] = array('Basic', 'CRUD', 'Simple');
        $data['title'] = 'Simple CRUD';

        $data['content'] = 'simple_crud/vindex';
        $this->load->view('template/vtemplate', $data);
    }

    public function form($id = 0) {

        if ($this->input->post('saveData')) {
            $input = $this->input->post();
            extract($input);

            $data = array(
                'nama_agama' => $nama_agama
            );

            $this->save($data, $id_agama);
            redirect('simple_crud');
        } else {
            $obj = new stdClass();
            $obj->id_agama = $id;
            $obj->nama_agama = '';


            if ($id != 0) {
                $obj = $this->Magama->by_id($id);
                $act = 'Edit';
            } else {
                $act = 'Add';
            }

            $data['data'] = $obj;
            $data['title'] = 'Simple CRUD';
            $data['breadcrumb'] = array('Basic', 'CRUD', 'Simple', $act);

            $data['content'] = 'simple_crud/vform';
            $this->load->view('template/vtemplate', $data);
        }
    }

    public function save($data, $id = 0) {
        $keterangan = ($id != 0) ? 'ubah' : 'tambah';
        $this->session->set_flashdata('notif', true);
        if ($this->Magama->save_data($data, $id)) {
            $this->session->set_flashdata('notif_class', 'alert-success');
            $this->session->set_flashdata('notif_title', 'Berhasil!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        } else {
            $this->session->set_flashdata('notif_class', 'alert-danger');
            $this->session->set_flashdata('notif_title', 'Gagal!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        }
    }

    public function delete($id) {

        $keterangan = 'hapus';
        $this->session->set_flashdata('notif', true);
        if ($this->Magama->delete($id)) {
            $this->session->set_flashdata('notif_class', 'alert-success');
            $this->session->set_flashdata('notif_title', 'Berhasil!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        } else {
            $this->session->set_flashdata('notif_class', 'alert-danger');
            $this->session->set_flashdata('notif_title', 'Gagal!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        }
        redirect('simple_crud');
    }

}
