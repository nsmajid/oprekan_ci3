<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Basic_upload extends CI_Controller {

    public function __construct() {
        parent::__construct();

        login_check();

        $this->load->model('Mtheory');
//        $this->load->helper(array('url','download'));	
    }

    public function index() {
        $data['theory'] = $this->Mtheory->all();
        $data['breadcrumb'] = array('Basic Basic', 'CRUD', 'Upload & Download');
        $data['title'] = 'Upload & Download';

        $data['content'] = 'basic_upload/vindex';
        $this->load->view('template/vtemplate', $data);
    }

    public function form($id = 0) {

        if ($this->input->post('saveData')) {
            $input = $this->input->post();
            extract($input);


            $data = array(
                'descriptions' => $descriptions,
                'file_theory' => $this->_uploadImage()
            );

            $this->save($data, $id_theory);
            redirect('basic_upload');
        } else {
            $obj = new stdClass();
            $obj->id_theory = $id;
            $obj->descriptions = '';
            $obj->file_theory = '';


            if ($id != 0) {
                $obj = $this->Mtheory->by_id($id);
                $act = 'Edit';
            } else {
                $act = 'Add';
            }

            $data['data'] = $obj;
            $data['title'] = 'Basic Upload & Download';
            $data['breadcrumb'] = array('Basic', 'CRUD', 'Upload & Download', $act);

            $data['content'] = 'basic_upload/vform';
            $this->load->view('template/vtemplate', $data);
        }
    }

    public function save($data, $id = 0) {
        $keterangan = ($id != 0) ? 'ubah' : 'tambah';
        $this->session->set_flashdata('notif', true);
        if ($this->Mtheory->save_data($data, $id)) {
            $this->session->set_flashdata('notif_class', 'alert-success');
            $this->session->set_flashdata('notif_title', 'Berhasil!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        } else {
            $this->session->set_flashdata('notif_class', 'alert-danger');
            $this->session->set_flashdata('notif_title', 'Gagal!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        }
    }

    public function delete($id = 0) {
        $this->_removeImage($id);
        
        $keterangan = 'hapus';
        $this->session->set_flashdata('notif', true);
        if ($this->Mtheory->delete($id)) {
            $this->session->set_flashdata('notif_class', 'alert-success');
            $this->session->set_flashdata('notif_title', 'Berhasil!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        } else {
            $this->session->set_flashdata('notif_class', 'alert-danger');
            $this->session->set_flashdata('notif_title', 'Gagal!');
            $this->session->set_flashdata('notif_message', 'Data berhasil di<b>' . $keterangan . '.</b>');
        }
        redirect('basic_upload');
    }

    public function download($id = 0) {

        $theory = $this->Mtheory->by_id($id);
        $file_name = $theory->file_theory;
        $data = file_get_contents(base_url('/includes/theory/' . $file_name));
        force_download($file_name, $data);
    }

    private function _uploadImage() {

        if (empty($_FILES["file_theory"]["name"])) {
            return $this->input->post('file_theory_old');
            ;
        }

        $config['upload_path'] = './includes/theory/';
        $config['allowed_types'] = 'pdf|jpg|png';
        $config['overwrite'] = true;
        $config['max_size'] = 10240; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file_theory')) {
            return $this->upload->data("file_name");
        }

//        print_r($this->upload->display_errors());

        return "default.jpg";
    }

    private function _removeImage($id) {
        $theory = $this->Mtheory->by_id($id);
        if ($theory->file_theory != "default.jpg" || $theory->file_theory != "" ) {
            $filename = explode(".", $theory->file_theory)[0];
            return array_map('unlink', glob(FCPATH . "includes/theory/$filename.*"));
        }
    }

}
