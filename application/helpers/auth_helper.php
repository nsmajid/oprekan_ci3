<?php

function login_check() {


    $ci = get_instance();

    if (!$ci->session->userdata('id_user')) {
        $ci->session->set_flashdata('notif', true);
        $ci->session->set_flashdata('notif_title', 'PERHATIAN !');
        $ci->session->set_flashdata('notif_message', 'Silahkan login dahulu.');
        $ci->session->set_flashdata('notif_class', 'danger');
        $ci->session->set_flashdata('notif_icon', 'fa fa-ban');

        redirect(base_url());
    }
}

function authorization($nama_priv = 0) {
    $ci = get_instance();


    $isadmin = $ci->session->userdata('isadmin');
  

    if (!$isadmin) {

        $priv = $ci->session->userdata('priv');
        
        if (!isset($priv[$nama_priv])) {
            redirect(base_url('autentikasi/page404'));
        }
    }
}
