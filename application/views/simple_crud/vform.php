<div class="row">
    <div class="col-md-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark"><?=$title?></span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">Class Simple_crud, form()</span>
                </h3>

            </div>
            <!--begin::Form-->
            <form method="POST">
                <div class="card-body">

                    <div class="form-group">
                        <label>Nama Agama
                            <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="nama_agama" value="<?=$data->nama_agama?>" placeholder="Enter name" required>

                    </div>


                </div>
                <div class="card-footer">
                    <input type="hidden"  name="id_agama" value="<?=$data->id_agama?>" >

                    <button type="submit" name="saveData" value="TRUE" class="btn btn-primary mr-2">Submit</button>
                    <button onclick="window.history.back();" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Card-->

    </div>
</div>