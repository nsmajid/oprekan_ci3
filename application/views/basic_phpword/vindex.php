<div class="row">
    <div class="col-lg-6">
        <!--begin::Callout-->
        <div class="card card-custom p-6 mb-8 mb-lg-0">
            <div class="card-body">
                <div class="row">
                    <!--begin::Content-->
                    <div class="col-sm-7">
                        <h2 class="text-dark mb-4"><?=$title?></h2>
                        <p class="text-dark-50 line-height-lg">Browser automatically download file word</p>
                    </div>
                    <!--end::Content-->
                    <!--begin::Button-->
                    <div class="col-sm-5 d-flex align-items-center justify-content-sm-end">
                        <a href="<?php echo base_url('basic_phpword/download') ?>" class="btn font-weight-bolder text-uppercase font-size-lg btn-primary py-3 px-6">Download</a>
                    </div>
                    <!--end::Button-->
                </div>
            </div>
        </div>
        <!--end::Callout-->
    </div>
</div>
