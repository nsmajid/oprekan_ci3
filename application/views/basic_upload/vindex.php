
<div class="row">
    <div class="col-xl-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <div class="card-header py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark"><?=$title?></span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">Class :: <code>Simple_crud</code>, <code>index()</code>, <code>delete()</code>, <code>_removeImage()</code></span>
                </h3>
                <div class="card-toolbar">
                    <a href="<?php echo base_url() ?>basic_upload/form" class="btn btn-success font-weight-bold mr-2">
                        <i class="fas fa-plus-square"></i> Add New
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!--begin::Example-->
                <div class="example mb-10">
                    <p>Menggunakan tabel 
                        <code>theory</code>. Load helper download : <code>$this->load->helper('download');</code></p>
                    <div class="example-preview">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col"></th>
                                    
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($theory as $i => $val){ ?>
                                <tr>
                                    <th scope="row"><?= $i+1 ?></th>
                                    
                                    <td><?= $val->descriptions ?></td>
                                    <td class="pr-0 text-right">
                                        <a href="<?= base_url().'basic_upload/download/'.$val->id_theory ?>" class="btn btn-clean btn-light btn-hover-success btn-sm btn-icon" >
                                            <i class="fas fa-file-download"></i>
                                        </a>
                                       

                                    </td>
                                    <td class="pr-0 text-right">
                                        <a href="<?= base_url().'basic_upload/form/'.$val->id_theory ?>" class="btn btn-clean btn-light btn-hover-warning btn-sm btn-icon" >
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="<?= base_url().'basic_upload/delete/'.$val->id_theory ?>" onclick="return confirm('are you sure?')" class="btn btn-clean btn-light btn-hover-danger btn-sm btn-icon" >
                                            <i class="fas fa-trash"></i>
                                        </a>

                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!--end::Card-->

    </div>

</div>