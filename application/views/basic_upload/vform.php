<div class="row">
    <div class="col-md-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark"><?= $title ?></span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">Class :: <code>Simple_crud</code>, <code>form()</code>, <code>_uploadImage()</code></span>
                </h3>

            </div>
            <!--begin::Form-->
            <form method="POST" enctype="multipart/form-data">
                <div class="card-body">

                    <div class="form-group">
                        <label>File
                            <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="file_theory">
                        <input type="hidden" class="form-control" name="file_theory_old" value="<?= $data->file_theory ?>">

                    </div>
                    <div class="form-group mb-1">
                        <label for="exampleTextarea">Descriptios
                            <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="descriptions"  rows="3" required><?= $data->descriptions ?></textarea>
                    </div>


                </div>
                <div class="card-footer">
                    <input type="hidden"  name="id_theory" value="<?= $data->id_theory ?>" >

                    <button type="submit" name="saveData" value="TRUE" class="btn btn-primary mr-2">Submit</button>
                    <button onclick="window.history.back();" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Card-->

    </div>
</div>