<?php

class Mtheory extends CI_Model {

    public function all($per_page = 0, $offset = 0, $order = 'id_theory', $set_order = 'ASC') {

        if ($per_page !== 0)
            $this->db->limit($per_page, $offset);

        $this->db->order_by($order, $set_order);

        return $this->db->get('theory')->result();
    }

    public function by_id($id) {
        $this->db->where('id_theory', $id);
        return $this->db->get('theory')->row();
    }

    function insert($data) {
        if ($this->db->insert('theory', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update($data, $id) {

        $this->db->where('id_theory', $id);
        if ($this->db->update('theory', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function delete($id) {
        if ($this->db->delete('theory', array('id_theory' => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function save_data($data, $id = 0) {
        if ($id != 0) {
            return $this->update($data, $id);
        } else {
            return $this->insert($data);
        }
    }

}
