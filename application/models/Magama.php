<?php

class Magama extends CI_Model {

  public function all($per_page = 0, $offset = 0, $order = 'nama_agama', $set_order = 'ASC') {
   
    if ($per_page !== 0)
      $this->db->limit($per_page, $offset);
    
    $this->db->order_by($order, $set_order);
      
    return $this->db->get('agama')->result();
  }
  
  public function by_id($id) {
    $this->db->where('id_agama', $id);
    return $this->db->get('agama')->row();
  }
  
   function insert($data) {
        if ($this->db->insert('agama', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update($data, $id) {

        $this->db->where('id_agama', $id);
        if ($this->db->update('agama', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
      function delete($id) {
        if ($this->db->delete('agama', array('id_agama' => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function save_data($data, $id=0)
	{
		if ($id != 0) {
			return $this->update($data, $id);
		} else {
			return $this->insert($data);
		}
	}
}
